#!/usr/bin/env python

import re
import sys
import config
import operator

import histogram

students = {}

# CONTRACT
# get_student :: string -> string
# PURPOSE
# Takes a log entry and extracts the user name.
# Looks like:
# SP13TAD265 - 1 February 2013, 07:38 AM - 98.23.24.93 - Student XYZ - course view - Spring 13 TAD 265 (Electricity and Electronics),  
# Where ' - ' means 'field separator'
fields = { 'course' : 0,
           'timestamp' : 1,
           'ipaddr' : 2,
           'name' : 3,
           'action' : 4,
           'title' : 5}
           
def grab (field, line):
    return line[fields[field]]

# CONTRACT
# generate_student_list :: (list-of strings) -> (list-of strings)
# PURPOSE
# Generates a unique list of students ignoring the people who
# are non-members of the class (instructors, administrators, etc.)
def generate_student_list (lines):
    all_students = []
    for line in lines:
        student = grab ('name', line)
        if (student not in all_students) and \
           (student not in config.non_members):
            all_students.append(student)
    return all_students

def sort_dict (d):
    return sorted(d.iteritems(), key=operator.itemgetter(1))

def count_total_interactions (users, lines):
    actions = {}
    for user in users:
        actions[user] = 0
    for line in lines:
        stu = grab('name', line)
        if stu in users:
            actions[stu] += 1
    return actions

# CONTRACT
# uniq_assignments :: (list-of strings) -> (list-of strings)
def uniq_assignments (lines):
    assignments = []
    for line in lines:
        result = re.search(grab('action', line), 'assignment add')
        if result:
            if grab('title', line) not in assignments:
                # Insert in order
                assignments.insert(0, grab('title', line))
    return assignments

def get_submissions_by_student (students, lines):
    subs = {}
    # Preload empty lists
    for stu in students:
        subs[stu] = []
    
    for line in lines:
        stu = grab('name', line)
        act = grab('action', line)
        if (act == 'assignment upload') \
           and (stu in students):
            assign = grab('title', line)
            if assign not in subs[stu]:
                # Add to the front, so we see assignments
                # in chronological order
                subs[stu].insert(0, assign)
    return subs

def hbar(char, count):
    for i in range(0, count):
        sys.stdout.write(char) 
    sys.stdout.flush()
     
def newline():
    print
            
def generate_reports (lines):
    students = generate_student_list(lines)
    
    total_interactions = count_total_interactions(students, lines)
    numbers = [(pair[1]) for pair in sort_dict(total_interactions)]
    
    all_assignments = uniq_assignments(lines)
    
    submissions = get_submissions_by_student(students, lines)
    
    for stu in students:
        hbar('*', 40)
        newline()
        
        print stu
        print "[TI] %s" % total_interactions[stu]
        print "[TS] %s of %s" % (len(submissions[stu]), len(all_assignments))
        print "Submitted:"
        for a in submissions[stu]:
            print "\t %s" % a
        
        hbar('-', 40)
        newline()
        newline()
        
        
    
        
# CONTRACT
# process :: string -> void
# PURPOSE
# Drive the processing of a Moodle logfile
# in CSV format.
def process (filename):
    fp = open (filename, 'r')
    lines = []
    for line in fp:
        lines.append([e.strip() for e in re.split('\t', line.strip())])
    # Remove the first two lines from the file
    lines.pop(0)
    lines.pop(0)
    fp.close()
    generate_reports(lines)

def main (cmdline):
    if len(cmdline) < 2:
        print "Usage:"
        print "process.py <logfile>.txt"
        sys.exit(-1)
    # Get the filename to process
    filename = sys.argv[1]
    
    # Do some error checking.
    
    # Process it
    process(filename)

if __name__ == "__main__":
    main(sys.argv)